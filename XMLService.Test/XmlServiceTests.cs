﻿using System;
using System.IO;
using NUnit.Framework;
using XMLCreator.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XMLService.Test
{
	[TestFixture]
	public class XmlServiceTests
	{
		private static readonly string pathToRepo =
			"C:/Users/Andrei_Kuchik/Documents/module7/XMLCreator/Template/XMLDoc.xml";

		private static readonly Book Book = new Book() { Id = 3, Title = "Book", City = "Minsk", Annotation = "qwsssa", Authors = new List<string> { "Kolia" }, Publishing = "ds", Description = "Book number 3" };
		private static readonly Magazine Magazine = new Magazine() { Id = 3, Title = "Magazine", City = "Minsk", Annotation = "qwsssa", Year = 2018, Description = "Book number 3" };
		private static readonly Patent Patent = new Patent() { Title = "Patent", Country = "Belarus", Annotation = "Annotation", RegisterNumber = 213, Description = "Description", DatePublication = DateTime.Now };

		[TestCase(null, null, typeof(IOException))]
		public void CreatingService_RepositoryNotFound_Exception(string filePath, Catalog catalog, Type exception)
		{
			Assert.Throws(exception, () => new XmlService());
		}

		[TestCase(typeof(ValidationException))]
		public void AddNewValueToRepository_InvalidItem(Type exception)
		{
			XmlService service = new XmlService(pathToRepo);
			Assert.Throws(exception, () => service.AddEntity(new Book() { Id = 3, Title = "", City = "Minsk", Annotation = "qwsssa", Authors = new List<string> { "Ivan" }, Publishing = "ds", Description = "Book number 4" }));
			Assert.Throws(exception, () => service.AddEntity(new Magazine() { Id = 3, Title = "Magazine", City = "Minsk", Annotation = "qwsssa", Year = 2019, Description = "gfhfgh" }));
			Assert.Throws(exception, () => service.AddEntity(new Patent() { Title = "Magazine", Country = "Belarus", Annotation = "qwsssa", RegisterNumber = 0, Description = "Description" }));

		}

		[TestCase()]
		public void AddNewValueToRepository_SuccessAddedNewBook()
		{
			XmlService service = new XmlService(pathToRepo);
			var operationResult = service.AddEntity(Book);
			Assert.AreEqual(operationResult, true);
		}

		[TestCase()]
		public void AddNewValueToRepository_SuccessAddedNewMagazine()
		{
			XmlService service = new XmlService(pathToRepo);
			var operationResult = service.AddEntity(Magazine);
			Assert.AreEqual(operationResult, true);
		}

		[TestCase()]
		public void AddNewValueToRepository_SuccessAddedNewPatent()
		{
			XmlService service = new XmlService(pathToRepo);
			var operationResult = service.AddEntity(Patent);
			Assert.AreEqual(operationResult, true);
		}

		[TestCase(typeof(ArgumentNullException))]
		public void AddNewValueToRepository_FailedAddedNewItem(Type exception)
		{
			XmlService service = new XmlService(pathToRepo);
			Assert.Throws(exception, () => service.AddEntity(null));
		}

		[TestCase(typeof(ArgumentNullException))]
		public void CheckEnumeration(Type exception)
		{
			XmlService service = new XmlService(pathToRepo);
			var operationResult = service.GetEnumerator();
			var book = operationResult.Current;
			Assert.AreEqual(book.Title, Book.Title);
			operationResult.MoveNext();
			var magazine = operationResult.Current;
			Assert.AreEqual(magazine.Title, Magazine.Title);
			operationResult.MoveNext();
			var patent = operationResult.Current;
			Assert.AreEqual(patent.Title, Patent.Title);
			
		}
	}
}


