﻿using System;
using System.IO;
using NUnit.Framework;
using XMLCreator;
using XMLCreator.Models;

namespace XMLService.Test
{
    [TestFixture]
    public class XmlRepositoryTest
    {
	    private static readonly string pathToRepo =
		    "C:/Users/Andrei_Kuchik/Documents/module7/XMLCreator/Template/XMLDoc.xml";

		[TestCase(null, typeof(ArgumentException)), TestCase("", typeof(ArgumentException))]
	    public void CreateRepositoryWithoutPath_Exception(string path, Type exception)
        {
            Assert.Throws(exception, () => new XmlRepository(path));
        }

        [TestCase("qwerty.xml", typeof(IOException))]
        public void GetStreamToRepository_RepositoryNotExsist_Exception(string repositoryName, Type exception)
        {
            XmlRepository repository = new XmlRepository(repositoryName);
            Assert.Throws(exception, () => repository.Load());
        }

        //Should be created repository
        [TestCase()]
        public void GetStreamToRepository_ReturnStreamToRepository()
        {
            XmlRepository repository = new XmlRepository(pathToRepo);
            Stream stream = repository.Load();
            Assert.AreNotEqual(stream, null);
        }

        //Should be created repository
        [TestCase( null, typeof(ArgumentNullException))]
        public void SaveDataInRepository_Exception(Catalog catalog, Type exception)
        {
            XmlRepository repository = new XmlRepository(pathToRepo);
            Assert.Throws(exception, () => repository.Save(catalog));
        }
    }
}
