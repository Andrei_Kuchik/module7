﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using XMLCreator;
using XMLCreator.Models;

namespace XMLService
{
	public class XmlService : IEnumerable<Entity>
	{
		private readonly XmlRepository repository;
		private readonly FileStream stream;
		private readonly XmlReader xmlReader;
		private readonly Dictionary<Type, XmlSerializerNamespaces> namepsaces;

		private const string endXml = "</Catalog>";

		public XmlService() : this(null, null)
		{
		}

		public XmlService(string pathFile) : this(pathFile, null)
		{
		}

		public XmlService(Catalog catalog) : this(null, catalog)
		{
		}

		public XmlService(string filePath, Catalog catalog)
		{
			var catalogName = new XmlQualifiedName("ctl", "http://LibraryStorage.Catalog/1.0.0.0");
			var bookName = new XmlQualifiedName("bk", "http://LibraryStorage.Book/1.0.0.0");
			var magazineName = new XmlQualifiedName("mgz", "http://LibraryStorage.Magazine/1.0.0.0");
			var patentName = new XmlQualifiedName("ptn", "http://LibraryStorage.Patent/1.0.0.0");

			namepsaces = new Dictionary<Type, XmlSerializerNamespaces>
			{
				{
					typeof(Catalog),
					new XmlSerializerNamespaces(new[] {catalogName, bookName, magazineName, patentName})
				},
				{
					typeof(Book), new XmlSerializerNamespaces(new[] {bookName})
				},
				{
					typeof(Patent), new XmlSerializerNamespaces(new[] {patentName})
				},
				{
					typeof(Magazine), new XmlSerializerNamespaces(new[] {magazineName})
				}
			};

			repository = string.IsNullOrWhiteSpace(filePath)
				? new XmlRepository("DefaultName.xml")
				: new XmlRepository(filePath);

			if (catalog != null)
			{
				repository.Save(catalog);
			}

			stream = repository.Load();
			stream.Seek(0, SeekOrigin.Begin);
			xmlReader = XmlReader.Create(stream, new XmlReaderSettings {Async = true, IgnoreWhitespace = true});
			xmlReader.ReadToFollowing("Catalog");
			xmlReader.Read();
		}

		public bool AddEntity(Entity entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException();
			}

			ValidateEntity(entity);

			stream.Position = stream.Length - endXml.Length;
			using (var writer = XmlWriter.Create(stream, new XmlWriterSettings
			{
				Indent = true,
				OmitXmlDeclaration = true
			}))
			{
				try
				{
					var serializer = new XmlSerializer(entity.GetType());
					serializer.Serialize(writer, entity, namepsaces[entity.GetType()]);
				}
				catch (Exception)
				{
					return false;
				}
			}

			var streamWriter = new StreamWriter(stream);
			streamWriter.Write($"\n{endXml}");
			streamWriter.Flush();
			stream.Seek(0, SeekOrigin.Begin);

			return true;
		}

		public IEnumerator<Entity> GetEnumerator()
		{
			return new Enumerator(this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private void ValidateEntity(Entity entity)
		{
			var validationContext = new ValidationContext(entity);
			Validator.ValidateObject(entity, validationContext, true);
		}

		private struct Enumerator : IEnumerator<Entity>
		{
			private readonly MemoryStream stream;
			private XmlReader xmlReader;

			public Enumerator(XmlService service)
			{
				stream = new MemoryStream();
				var position = service.stream.Position;
				service.stream.Seek(0, SeekOrigin.Begin);
				service.stream.CopyTo(stream);
				service.stream.Position = position;
				xmlReader = null;
				Reset();
			}

			public Entity Current
			{
				get
				{
					Entity entity = null;
					xmlReader.ReadSubtree();
					switch (xmlReader.LocalName)
					{
						case "Patent":
							entity = (Patent) new XmlSerializer(typeof(Patent)).Deserialize(xmlReader);
							break;
						case "Magazine":
							entity = (Magazine)new XmlSerializer(typeof(Magazine)).Deserialize(xmlReader);
							break;
						case "Book":
							try
							{
								entity = (Book)new XmlSerializer(typeof(Book)).Deserialize(xmlReader);

							}
							catch (Exception e)
							{
								Console.WriteLine(e);
							}
							break;
					}

					return entity;
				}
			}

			object IEnumerator.Current => Current;

			public void Dispose()
			{
				stream.Dispose();
				xmlReader.Dispose();
			}

			public bool MoveNext()
			{
				while (!xmlReader.EOF)
				{
					if (xmlReader.NodeType == XmlNodeType.Element &&
					    (xmlReader.LocalName == "Book" || xmlReader.LocalName == "Patent" ||
					     xmlReader.LocalName == "Magazine"))
					{
						return true;
					}

					if (xmlReader.NodeType == XmlNodeType.EndElement && xmlReader.LocalName == "Catalog")
					{
						return false;
					}
				}

				return false;
			}

			public void Reset()
			{
				stream.Seek(0, SeekOrigin.Begin);
				xmlReader = XmlReader.Create(stream, new XmlReaderSettings {Async = true, IgnoreWhitespace = true});
				xmlReader.ReadToFollowing("Catalog");
				xmlReader.Read();
			}
		}
	}
}