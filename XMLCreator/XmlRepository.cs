﻿using System;
using System.IO;
using System.Xml.Serialization;
using XMLCreator.Models;

namespace XMLCreator
{
	public class XmlRepository : IXmlRepository
	{
		private string FilePath { get; }

		public XmlRepository(string filePath)
		{
			if (string.IsNullOrEmpty(filePath))
			{
				throw new ArgumentException();
			}

			FilePath = filePath;
		}

		public FileStream Load()
		{
			FileStream fileStream;
			try
			{
				fileStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite);
			}
			catch (IOException)
			{
				throw new IOException($"Cannot load {FilePath} this file");
			}

			return fileStream;
		}

		public void Save(Catalog catalog)
		{
			if (catalog == null)
			{
				throw new ArgumentNullException();
			}

			try
			{
				using (var fileStream = new FileStream(FilePath, FileMode.Create, FileAccess.Write))
				{
					var serializer = new XmlSerializer(typeof(Catalog));
					serializer.Serialize(fileStream, catalog);
				}
			}
			catch (IOException)
			{
				throw new IOException("Cannot save current data");
			}
		}
	}
}