﻿using System.IO;
using XMLCreator.Models;

namespace XMLCreator
{
	public interface IXmlRepository
	{
		void Save(Catalog catalog);
		FileStream Load();
	}
}