﻿using System;
using PostSharp.Aspects;

namespace XMLCreator.Attribute
{
	internal class RangeAttribute : LocationInterceptionAspect
	{
		private readonly int min;
		private readonly int max;

		public RangeAttribute(int min, int max)
		{
			this.min = min;
			this.max = max;
		}

		public override void OnSetValue(LocationInterceptionArgs args)
		{
			var value = (int) args.Value;
			if (value < min)
			{
				value = min;
			}

			if (value > max)
			{
				value = max;
			}

			args.SetNewValue(value);
		}
	}
}