﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace XMLCreator.Models
{
	public class Entity
	{
		[Required]
		[XmlElement(IsNullable = false)]
		public string Title { get; set; }

		[Required]
		[XmlAttribute]
		public string Description { get; set; }

	}
}
