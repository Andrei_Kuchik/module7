﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace XMLCreator.Models
{
	[XmlRoot]
	public class Book : Entity
	{
		[XmlAttribute("ISBN")]
		public int Id { get; set; }

		[XmlArray("Authors"), XmlArrayItem("Author", typeof(string))]
		public List<string> Authors { get; set; }

		[XmlElement]
		public string City { get; set; }

		[XmlElement]
		public string Publishing { get; set; }

		[XmlElement]
		public string Annotation { get; set; }

		[XmlElement]
		public int CountPage { get; set; }

	}
}
