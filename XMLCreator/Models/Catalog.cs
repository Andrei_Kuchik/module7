﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace XMLCreator.Models
{
	[XmlRoot(IsNullable = false)]
	public class Catalog
	{
		[XmlAttribute]
		public string Library { get; set; }

		[XmlAttribute]
		public DateTime DateCreated { get; set; }

		[XmlElement("Book")]
		public List<Book> Books { get; set; }

		[XmlElement("Magazin")]
		public List<Magazine> Magazins { get; set; }

		[XmlElement("Patent")]
		public List<Patent> Patents { get; set; }
	}
}
