﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace XMLCreator.Models
{
	[XmlRoot]
	public class Patent : Entity
	{
		[XmlElement]
		public List<string> Creators { get; set; }

		[XmlElement]
		public string Country { get; set; }

		[Range(1, int.MaxValue)]
		[XmlElement(IsNullable = false)]
		public int RegisterNumber { get; set; }

		[XmlElement]
		public DateTime DateRequest { get; set; }

		[XmlElement]
		public DateTime DatePublication { get; set; }

		[XmlElement]
		public string Annotation { get; set; }

		[XmlElement]
		public int CountPage { get; set; }
	}
}
