﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace XMLCreator.Models
{
	[XmlRoot]
	public class Magazine : Entity
	{
		[XmlAttribute("ISSN")]
		public int Id { get; set; }

		[XmlElement]
		public string City { get; set; }

		[XmlElement]
		[Range(1, 2018)]
		public int Year {get; set;}

		[XmlElement]
		public string Annotation { get; set; }

		[XmlElement(IsNullable = false)]
		public int Issue { get; set; }

		[XmlElement]
		public DateTime Date { get; set; }

		[XmlElement]
		public int CountPage { get; set; }
	}
}
